# An unofficial implementation of stochastic multiple choice learning
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Implementation of feed-forward neural network"""
from typing import List, Tuple

import torch


class DNN(torch.nn.Module):
    """
    A feed-forward neural network that optimizes
    softmax cross entropy using Adam optimizer.

    """

    def __init__(
        self,
        units: List or Tuple = [(784, 500), (500, 500), (500, 10)],
        learning_rate: float = 1e-3,
        device: torch.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        ),
    ):
        """
        Constructs a feed-forward neural network classifier.

        Parameters
        ----------
        units: list or tuple
            An iterable that consists of the number of units in each hidden layer.
        learning_rate: float
            The learning rate to use for optimization.
        device: torch.device
            The device to use for model computations.
        """
        super().__init__()
        self.layers = torch.nn.ModuleList(
            [
                torch.nn.Linear(in_features=in_features, out_features=out_features)
                for in_features, out_features in units
            ]
        )
        self.optimizer = torch.optim.SGD(
            params=self.parameters(), lr=learning_rate, momentum=9e-1, weight_decay=1e-5
        )
        self.criterion = torch.nn.CrossEntropyLoss()
        self.train_loss = []
        self.train_accuracy = []
        self.device = device
        self.to(self.device)

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        Defines the forward pass by the model.

        Parameter
        ---------
        features : torch.Tensor
            The input features.
        Returns
        -------
        logits : torch.Tensor
            The model output.
        """
        if len(features.shape) > 2:
            features = features.view(features.shape[0], -1)
        activations = {}
        for index, layer in enumerate(self.layers):
            if index == 0:
                activations[index] = torch.relu(layer(features))
            elif index == len(self.layers) - 1:
                activations[index] = layer(activations.get(index - 1))
            else:
                activations[index] = torch.relu(layer(activations.get(index - 1)))
        logits = activations.get(len(activations) - 1)
        return logits

    def epoch_train(
        self, data_loader: torch.utils.data.DataLoader
    ) -> Tuple[float, float]:
        """
        Trains the model for one epoch.

        Parameters
        ----------
        data_loader : torch.utils.dataloader.DataLoader
            The data loader object that consists of the data pipeline.

        Returns
        -------
        epoch_loss: float
            The epoch loss.
        epoch_accuracy: float
            The epoch accuracy.
        """
        epoch_loss = 0
        epoch_accuracy = 0
        for batch_features, batch_labels in data_loader:
            batch_features = batch_features.to(self.device)
            batch_labels = batch_labels.to(self.device)
            self.optimizer.zero_grad()
            outputs = self(batch_features)
            train_loss = self.criterion(outputs, batch_labels)
            train_loss.backward()
            self.optimizer.step()
            epoch_loss += train_loss.item()
            accuracy = (outputs.argmax(1) == batch_labels).sum().item() / len(
                batch_labels
            )
            epoch_accuracy += accuracy
        epoch_loss /= len(data_loader)
        epoch_accuracy /= len(data_loader)
        return epoch_loss, epoch_accuracy

    def fit(
        self, data_loader: torch.utils.data.DataLoader, epochs: int, show_every: int = 2
    ) -> None:
        """
        Trains the FFNN model.

        Parameters
        ----------
        data_loader: torch.utils.dataloader.DataLoader
            The data loader object that consists of the data pipeline.
        epochs: int
            The number of epochs to train the model.
        show_every: int
            The interval in terms of epoch on displaying training progress.
        """
        for epoch in range(epochs):
            epoch_loss, epoch_accuracy = self.epoch_train(data_loader)
            self.train_loss.append(epoch_loss)
            self.train_accuracy.append(epoch_accuracy)
            if epoch % show_every == 0:
                print(
                    f"epoch {epoch + 1}/{epochs} : mean loss = {epoch_loss:.4f}\t|\tmean acc = {epoch_accuracy:.4f}"
                )
