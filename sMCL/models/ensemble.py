# An unofficial implementation of stochastic multiple choice learning
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Implementation of ensemble neural network"""
from copy import deepcopy
import os
import time
from typing import Dict, Tuple

from soconne_baseline import ResNet18
import torch
import torch.nn as nn
import torch.optim as optim


class Ensemble(nn.Module):
    def __init__(
        self,
        network: nn.Module,
        num_features: int = 784,
        input_shape: Tuple = (28, 28),
        num_classes: int = 10,
        num_learners: int = 3,
        device: torch.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        ),
        use_feature_extractor: bool = False,
        code_dim: int = 256,
        optimizer: str = "sgd",
        learning_rate: float = 1e-1,
        weight_decay: float = 1e-5,
    ):
        """
        Constructs an ensemble of neural networks.

        Parameters
        ----------
        network: torch.nn.Module
            The learner to use in the ensemble.
        num_features: int
            The number of input features dimensionality.
        input_shape: Tuple
            The full shape of input features.
        num_classes: int
            The number of classes.
        num_learners: int
            The number of networks to use in ensemble.
        device: torch.device
            The device to use for computations.
        use_feature_extractor: bool
            Whether to use feature extractor or not.
        code_dim: int
            The dimensionality of the extracted features.
        optimizer: str
            The optimization algorithm to use.
            Options: [sgd (default) | adamw]
        learning_rate: float
            The learning rate to use for optimization.
        weight_decay: float
            The decay factor to use for regularization.
        """
        super().__init__()
        self.use_feature_extractor = use_feature_extractor
        if self.use_feature_extractor:
            model = ResNet18(
                blocks_to_freeze=3,
                use_pretrained_cifar10=True,
                input_shape=input_shape,
                num_classes=10,
            )
            model = torch.nn.Sequential(*(list(model.resnet.children())[:-1]))
            self.feature_extractor = torch.nn.Sequential(
                model,
                torch.nn.Flatten(),
                torch.nn.Linear(in_features=512, out_features=code_dim),
                torch.nn.ReLU(inplace=True),
            )
        self.num_learners = num_learners
        self.model = nn.Sequential()
        for index in range(self.num_learners):
            network = network.to(device)
            network = deepcopy(network)
            if not network.__class__.__name__.lower().__contains__("resnet"):
                network.apply(self.reset_parameters)
            self.model.add_module(f"network_{index}", network)
        self.criterion = nn.CrossEntropyLoss()
        if optimizer == "sgd":
            self.optimizer = optim.SGD(
                params=self.parameters(),
                lr=learning_rate,
                momentum=9e-1,
                weight_decay=weight_decay,
            )
        elif optimizer == "adamw":
            self.optimizer = torch.optim.AdamW(
                params=filter(
                    lambda parameters: parameters.requires_grad, self.parameters()
                ),
                lr=learning_rate,
                weight_decay=weight_decay,
            )
        self.scheduler = optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, patience=2, min_lr=1e-4, verbose=True, factor=1e-1
        )
        self.train_loss = []
        self.train_accuracy = []
        self.valid_loss = []
        self.valid_accuracy = []
        self.device = device
        self.to(self.device)

    def reset_parameters(self, modules: nn.Module):
        if isinstance(modules, torch.nn.Linear) or isinstance(modules, torch.nn.Conv2d):
            modules.reset_parameters()

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        The forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        logits: torch.Tensor
            The model outputs.
        """
        if self.use_feature_extractor:
            features = self.feature_extractor(features)
        outputs = []
        for network in self.model:
            outputs.append(network(features))
        outputs = torch.stack(outputs, dim=1)
        logits = torch.sum(outputs, dim=1)
        return logits, outputs

    def epoch_train(self, data_loaders: Dict, phase: str) -> Tuple[float, float]:
        """
        Defines the training procedure per epoch.

        Parameters
        ----------
        data_loaders: Dict
            The data loaders to use for
            training and validation.
        phase: str
            The phase of training,
            whether training or validation.

        Returns
        -------
        Tuple[float, float]
            epoch_loss: float
                The training or validation epoch loss.
            epoch_accuracy: float
                The training or validation epoch accuracy.
        """
        epoch_loss = 0.0
        epoch_accuracy = 0.0
        for features, labels in data_loaders.get(phase):
            learner_loss = torch.zeros((self.num_learners))
            features = features.to(self.device)
            labels = labels.to(self.device)

            self.optimizer.zero_grad()

            with torch.set_grad_enabled(phase == "train"):
                outputs, learner_outputs = self(features)
                for index, output in enumerate(learner_outputs.permute(1, 0, 2)):
                    train_loss = self.criterion(output, labels)
                    learner_loss[index] = train_loss
                _, learner_index = learner_loss.topk(1, largest=False)

                self.freeze_learners(winner_learner_index=learner_index)

                if phase == "train":
                    learner_loss[learner_index].backward()
                    self.optimizer.step()

            epoch_loss += learner_loss[learner_index].item()
            accuracy = (outputs.argmax(1) == labels).sum().item() / len(labels)
            epoch_accuracy += accuracy

            self.unfreeze_learners(winner_learner_index=learner_index)
        epoch_loss /= len(data_loaders.get(phase))
        epoch_accuracy /= len(data_loaders.get(phase))
        return epoch_loss, epoch_accuracy

    def fit(
        self,
        train_loader: torch.utils.data.DataLoader,
        valid_loader: torch.utils.data.DataLoader,
        epochs: int,
        show_every: int = 2,
    ):
        """
        Trains the model for a given epochs.

        Parameters
        ----------
        train_loader: torch.utils.data.DataLoader
            The training data loader to use.
        valid_loader: torch.utils.data.DataLoader
            The validation data loader to use.
        epochs: int
            The number of epochs to train the model.
        show_every: int
            The interval between training results displays.
        """
        start_time = time.time()
        best_model_weights = deepcopy(self.state_dict())
        best_accuracy = 0.0

        data_loaders = dict()
        data_loaders["train"] = train_loader
        data_loaders["valid"] = valid_loader

        for epoch in range(epochs):
            if (epoch + 1) % show_every == 0:
                print()
                print(f"Epoch {epoch + 1}/{epochs}")
                print("-" * 40)
            for phase in data_loaders.keys():
                if phase == "train":
                    self.train()
                elif phase == "valid":
                    self.eval()

                epoch_loss, epoch_accuracy = self.epoch_train(data_loaders, phase)

                if phase == "train":
                    self.scheduler.step(epoch)
                    self.train_loss.append(epoch_loss)
                    self.train_accuracy.append(epoch_accuracy)
                elif phase == "valid":
                    self.valid_loss.append(epoch_loss)
                    self.valid_accuracy.append(epoch_accuracy)

                if (epoch + 1) % show_every == 0:
                    print(
                        f"{phase.title()} Loss: {epoch_loss:.4f} Acc: {epoch_accuracy:.4f}"
                    )

                if phase == "valid" and epoch_accuracy > best_accuracy:
                    best_accuracy = epoch_accuracy
                    best_model_weights = deepcopy(self.state_dict())
        time_elapsed = time.time() - start_time
        print()
        print(
            f"Training complete in {(time_elapsed // 60):.0f}m {(time_elapsed % 60):.0f}s"
        )
        print(f"Best Validation Accuracy: {best_accuracy:.4f}")
        print()

        self.load_state_dict(best_model_weights)

    def freeze_learners(self, winner_learner_index: int) -> None:
        """
        Freezes the parameters of the losing learners.

        Parameter
        ---------
        winner_learner_index: int
            The index of the winning learner.
        """
        for index, learner in enumerate(self.model):
            if index != winner_learner_index:
                for param in learner.parameters():
                    param.requires_grad = False

    def unfreeze_learners(self, winner_learner_index: int) -> None:
        """
        Unfreezes the parameters of the losing learners.

        Parameter
        ---------
        winner_learner_index: int
            The index of the winning learner.
        """
        for index, learner in enumerate(self.model):
            if index != winner_learner_index:
                for param in learner.parameters():
                    param.requires_grad = True

    def score(self, data_loader: torch.utils.data.DataLoader) -> float:
        """
        Returns the test accuracy of the model.

        Parameter
        ---------
        data_loader: torch.utils.data.DataLoader
            The data loader to use for evaluating the model.
        """
        self.eval()
        with torch.no_grad():
            for features, labels in data_loader:
                features = features.to(self.device)
                labels = labels.to(self.device)
                outputs, _ = self(features)
        accuracy = (outputs.argmax(1) == labels).sum().item() / len(labels)
        accuracy *= 100.0
        return accuracy

    def save_model(self, filename: str) -> None:
        """
        Exports the trained model to
        `outputs/models` directory.

        Parameter
        ---------
        filename: str
            The filename for the exported model.
        """
        print("[INFO] Exporting trained model...")
        model_name = filename.split("-")[2]
        dataset_name = filename.split("-")[5]
        model_path = os.path.join("outputs", "models", model_name, dataset_name)
        if not os.path.exists(model_path):
            os.makedirs(model_path)
        filename = f"{filename}.pth"
        filename = os.path.join(model_path, filename)
        torch.save(self.state_dict(), filename)
        print(f"[SUCCESS] Trained model exported to {filename}")

    def load_model(self, filename: str) -> None:
        """
        Loads the trained model from
        `outputs/models` directory.

        Parameter
        ---------
        filename: str
            The filename for the exported model to load.
        """
        print("[INFO] Loading the trained model...")
        model_name = filename.split("-")[2]
        dataset_name = filename.split("-")[5]
        model_path = os.path.join("outputs", "models", model_name, dataset_name)
        if not filename.endswith(".pth"):
            filename = f"{filename}.pth"
        filename = os.path.join(model_path, filename)
        if os.path.isfile(filename):
            self.load_state_dict(torch.load(filename))
            print("[SUCCESS] Trained model ready for use.")
        else:
            print("[ERROR] Trained model not found.")
