# An unofficial implementation of stochastic multiple choice learning
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""CNN Ensemble sMCL classifier"""
import argparse
from math import ceil, floor

import numpy as np
from pt_datasets import load_dataset, create_dataloader
import torch

from sMCL.models import CNN, Ensemble
from sMCL.utils import set_global_seed


def main(arguments):
    (
        seeds,
        dataset,
        batch_size,
        learning_rate,
        weight_decay,
        epochs,
        num_learners,
        show_every,
    ) = (
        arguments.seeds,
        arguments.dataset,
        arguments.batch_size,
        arguments.learning_rate,
        arguments.weight_decay,
        arguments.epochs,
        arguments.num_learners,
        arguments.show_every,
    )

    for num_learner in range(2, num_learners + 1):
        accuracies = []
        for seed in seeds:
            print()
            print(f"[INFO] Dataset: {dataset}")
            print(f"[INFO] Number of learners: {num_learner}")
            print(f"[INFO] Seed: {seed}")

            set_global_seed(seed=seed)

            train_data, test_data = load_dataset(dataset, normalize=False)

            input_shape = train_data.data.shape
            num_features = np.prod(train_data.data.shape[1:])
            num_classes = len(train_data.classes)

            train_data, valid_data = torch.utils.data.random_split(
                train_data,
                [ceil(len(train_data) * 0.90), floor(len(train_data) * 0.10)],
                generator=torch.Generator().manual_seed(seed),
            )

            train_loader = create_dataloader(
                train_data, batch_size=batch_size, num_workers=4
            )
            valid_loader = create_dataloader(
                valid_data, batch_size=batch_size, num_workers=4
            )
            test_loader = create_dataloader(
                test_data, batch_size=len(test_data), num_workers=4
            )
            learner = CNN(
                dim=input_shape[1],
                input_dim=(1 if len(input_shape) < 4 else input_shape[3]),
                num_classes=num_classes,
            )
            model = Ensemble(
                num_features=num_features,
                input_shape=input_shape,
                num_classes=num_classes,
                network=learner,
                num_learners=num_learner,
                learning_rate=learning_rate,
                weight_decay=weight_decay,
            )
            model.fit(train_loader, valid_loader, epochs=epochs, show_every=show_every)
            accuracy = model.score(test_loader)
            accuracies.append(accuracy)
            print(f"Test Accuracy: {accuracy:.4f}")
        print()
        print("=" * 40)
        print(f"AVG ACC = {np.mean(accuracies):.4f}")
        print(f"MAX ACC = {np.max(accuracies):.4f}")
        print(f"STDDEV ACC = {np.std(accuracies):.4f}")
        print("=" * 40)


def parse_args():
    parser = argparse.ArgumentParser(description="Stochastic Multiple Choice Learning")
    group = parser.add_argument_group("Parameters")
    group.add_argument(
        "-s",
        "--seeds",
        nargs="+",
        type=int,
        required=False,
        default=[1234, 42, 73],
        help="the list of random seeds to use, default: [1234, 42, 73]",
    )
    group.add_argument(
        "-d",
        "--dataset",
        type=str,
        default="mnist",
        help="the dataset to use, default: [mnist]",
    )
    group.add_argument(
        "-b",
        "--batch_size",
        type=int,
        default=200,
        help="the mini-batch size to use, default: [200]",
    )
    group.add_argument(
        "-lr",
        "--learning_rate",
        type=float,
        default=1e-1,
        help="the learning rate to use for optimization, default: [1e-1]",
    )
    group.add_argument(
        "-wd",
        "--weight_decay",
        type=float,
        default=1e-5,
        help="the weight decay to use during optimization, default: [1e-5]",
    )
    group.add_argument(
        "-e",
        "--epochs",
        type=int,
        default=10,
        help="the number of epochs to train the model, default: [10]",
    )
    group.add_argument(
        "-nl",
        "--num_learners",
        type=int,
        default=3,
        help="the number of learners to instantiate, default: [3]",
    )
    group.add_argument(
        "-se",
        "--show_every",
        type=int,
        default=1,
        help="the number of interval between training progress displays, default: [1]",
    )
    arguments = parser.parse_args()
    return arguments


if __name__ == "__main__":
    arguments = parse_args()
    main(arguments)
