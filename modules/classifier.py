import argparse

import numpy as np
from soconne_baseline import ResNet18, ResNet34, ResNet50
import torch

from sMCL.models import CNN, DNN, Ensemble, LeNet
from sMCL.utils import (
    create_dataloaders,
    export_results,
    get_ensemble_filename,
    set_global_seed,
)


def main(arguments):
    (
        seeds,
        dataset,
        normalize,
        vectorizer,
        ngram_range,
        batch_size,
        optimizer,
        learning_rate,
        weight_decay,
        epochs,
        num_subnetworks,
        subnetwork_architecture,
        show_every,
        use_pretrained_cifar10,
        num_blocks_to_freeze,
        cuda_index,
    ) = (
        arguments.seeds,
        arguments.dataset,
        arguments.normalize,
        arguments.vectorizer,
        arguments.ngram_range,
        arguments.batch_size,
        arguments.optimizer,
        arguments.learning_rate,
        arguments.weight_decay,
        arguments.epochs,
        arguments.num_subnetworks,
        arguments.subnetwork_architecture,
        arguments.show_every,
        arguments.use_pretrained_cifar10,
        arguments.num_blocks_to_freeze,
        arguments.cuda_device,
    )

    cuda_index = 0 if torch.cuda.device_count() == 1 else cuda_index
    device = torch.device(f"cuda:{cuda_index}" if torch.cuda.is_available() else "cpu")

    results = dict()

    for num_subnetwork in range(2, num_subnetworks + 1):
        accuracies = []
        for seed in seeds:
            print()
            print(f"[INFO] Dataset: {dataset}")
            print(f"[INFO] Number of sub-networks: {num_subnetwork}")
            print(f"[INFO] Seed: {seed}")

            set_global_seed(seed=seed)
            data_loaders = create_dataloaders(
                dataset=dataset,
                vectorizer=vectorizer,
                ngram_range=ngram_range,
                batch_size=batch_size,
                seed=seed,
                normalize=normalize,
            )

            train_loader = data_loaders.get("train")
            valid_loader = data_loaders.get("valid")
            test_loader = data_loaders.get("test")
            num_features = data_loaders.get("meta").get("num_features")
            input_shape = data_loaders.get("meta").get("input_shape")
            num_classes = data_loaders.get("meta").get("num_classes")

            if subnetwork_architecture == "dnn":
                subnetwork = DNN(
                    units=((num_features, 100), (100, num_classes)), device=device
                )
            elif subnetwork_architecture == "cnn":
                subnetwork = CNN(
                    dim=input_shape[1],
                    input_dim=(1 if len(input_shape) < 4 else input_shape[3]),
                    num_classes=num_classes,
                    device=device,
                )
            elif subnetwork_architecture == "lenet":
                subnetwork = LeNet(
                    dim=input_shape[1],
                    channel_dim=(1 if len(input_shape) < 4 else input_shape[3]),
                    num_classes=num_classes,
                    device=device,
                )
            elif subnetwork_architecture == "resnet18":
                subnetwork = ResNet18(
                    input_shape=input_shape,
                    num_classes=num_classes,
                    learning_rate=learning_rate,
                    blocks_to_freeze=num_blocks_to_freeze,
                    use_pretrained_cifar10=use_pretrained_cifar10,
                    device=device,
                )
            elif subnetwork_architecture == "resnet34":
                subnetwork = ResNet34(
                    input_shape=input_shape,
                    num_classes=num_classes,
                    learning_rate=learning_rate,
                    blocks_to_freeze=num_blocks_to_freeze,
                    use_pretrained_cifar10=use_pretrained_cifar10,
                    device=device,
                )
            elif subnetwork_architecture == "resnet50":
                subnetwork = ResNet50(
                    input_shape=input_shape,
                    num_classes=num_classes,
                    learning_rate=learning_rate,
                    blocks_to_freeze=num_blocks_to_freeze,
                    use_pretrained_cifar10=use_pretrained_cifar10,
                    device=device,
                )

            model = Ensemble(
                num_features=num_features,
                input_shape=input_shape,
                num_classes=num_classes,
                network=subnetwork,
                num_learners=num_subnetwork,
                optimizer=optimizer,
                learning_rate=learning_rate,
                weight_decay=weight_decay,
                device=device,
            )
            model.fit(train_loader, valid_loader, epochs=epochs, show_every=show_every)
            accuracy = model.score(test_loader)
            accuracies.append(accuracy)
            results[f"acc_seed_{seed}"] = accuracy
            print(f"Test Accuracy {accuracy:.4f}")
            filename = get_ensemble_filename(
                num_subnetwork=num_subnetwork,
                subnetwork_architecture=subnetwork_architecture,
                dataset=dataset,
                learning_rate=learning_rate,
                optimizer=optimizer,
                batch_size=batch_size,
            )
            model.save_model(filename=f"{seed}-seed-{filename}")
        print()
        print("=" * 40)
        avg_acc = np.mean(accuracies)
        max_acc = np.max(accuracies)
        std_acc = np.std(accuracies)
        print(f"Number of sub-networks: {num_subnetwork}")
        print(f"AVG ACC = {avg_acc:.4f}")
        print(f"MAX ACC = {max_acc:.4f}")
        print(f"STDDEV ACC = {std_acc:.4f}")
        print("=" * 40)
        results["acc_avg"] = avg_acc
        results["acc_max"] = max_acc
        results["acc_std"] = std_acc
        export_results(model_results=results, filename=filename)


def parse_args():
    parser = argparse.ArgumentParser(
        description="Ensemble classifier using stochastic multiple choice learning."
    )
    group = parser.add_argument_group("Parameters")
    group.add_argument(
        "-s",
        "--seeds",
        nargs="+",
        type=int,
        required=False,
        default=[1234, 42, 73],
        help="the list of random seeds to use, default: [1234, 42, 73]",
    )
    group.add_argument(
        "-d",
        "--dataset",
        type=str,
        default="mnist",
        help="the dataset to use, default: [mnist]",
    )
    group.add_argument(
        "--normalize", required=False, dest="normalize", action="store_true"
    )
    group.add_argument(
        "-v",
        "--vectorizer",
        type=str,
        default="ngrams",
        help="the vectorizer to use, options: [ngrams (default) | tfidf]",
    )
    group.add_argument(
        "-nr",
        "--ngram_range",
        nargs="+",
        required=False,
        default=(1, 5),
        type=int,
        help="the n-grams range to use for text vectorization, default: [(1, 5)]",
    )
    group.add_argument(
        "-b",
        "--batch_size",
        type=int,
        default=256,
        help="the mini-batch size to use, default: [256]",
    )
    group.add_argument(
        "--optimizer",
        type=str,
        default="sgd",
        help="the optimization algorithm to use, options: [sgd (default) | adamw]",
    )
    group.add_argument(
        "-lr",
        "--learning_rate",
        type=float,
        default=3e-4,
        help="the learning rate to use for optimization, default: [3e-4]",
    )
    group.add_argument(
        "-wd",
        "--weight_decay",
        type=float,
        default=1e-5,
        help="the weight decay to use during optimization, default: [1e-5]",
    )
    group.add_argument(
        "-e",
        "--epochs",
        type=int,
        default=10,
        help="the number of epochs to train the model, default: [10]",
    )
    group.add_argument(
        "-se",
        "--show_every",
        type=int,
        default=1,
        help="the number of interval between training progress displays, default: [1]",
    )
    group.add_argument(
        "-nl",
        "--num_subnetworks",
        type=int,
        default=3,
        help="the number of learners to instantiate, default: [3]",
    )
    group.add_argument(
        "--subnetwork_architecture",
        type=str,
        default="dnn",
        help="the architecture to use for an expert, options: [cnn | dnn (default) | lenet]",
    )
    group.add_argument(
        "--use_pretrained_cifar10",
        required=False,
        dest="use_pretrained_cifar10",
        action="store_true",
    )
    group.add_argument(
        "--num_blocks_to_freeze",
        type=int,
        default=4,
        help="the number of ResNet blocks to freeze, default: [4]",
    )
    group.add_argument(
        "--cuda_device",
        type=int,
        default=0,
        help="the index of CUDA device to use for computations, default: [0]",
    )
    group.set_defaults(normalize=False)
    group.set_defaults(use_feature_extractor=False)
    group.set_defaults(use_pretrained_cifar10=False)
    arguments = parser.parse_args()
    return arguments


if __name__ == "__main__":
    arguments = parse_args()
    main(arguments)
