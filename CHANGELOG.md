# Changelog

We track the notable changes to this project using the conventional commit messages in the `main` branch.

## Versions

## Chores

- 7382626 - chore: Update poetry lock file to reflect update of soconne-baseline (Mon, 8 May 2023 03:35:03 +0800)
- 27fbfbe - chore: Use master branch of soconne-baseline private repo (Mon, 8 May 2023 03:34:51 +0800)
- 092b7d6 - chore: Remove primitive install method of setup.py (Sun, 7 May 2023 22:19:46 +0800)
- 981c042 - chore: Update poetry lock file to reflect addition of torchvision to deps (Sun, 7 May 2023 22:10:31 +0800)
- c4c82af - chore: Add torchvision to list of dependencies (Sun, 7 May 2023 22:10:19 +0800)
- 09ac28d - chore: Update poetry lock file to reflect migration to torch 2.0 (Sun, 7 May 2023 22:08:24 +0800)
- 5cf5c7e - chore: Migrate to PyTorch 2.0 (Sun, 7 May 2023 22:08:08 +0800)
- d103f49 - chore: Remove huggingface datasets from deps (Sun, 7 May 2023 18:24:25 +0800)
- 9147cea - chore: Update poetry lock file to reflect changes in pyproject.toml (Sun, 7 May 2023 15:49:51 +0800)
- 9abd017 - chore: Add vision extras for huggingface datasets package (Sun, 7 May 2023 15:49:39 +0800)
- 1c7415c - chore: Add huggingface datasets to deps (Sun, 7 May 2023 15:45:40 +0800)
- ea0fbbd - chore: Update pre-commit config file (Sun, 7 May 2023 15:37:02 +0800)
- 48e35bc - chore: Update poetry lock file to reflect changes in pyproject.toml (Sun, 7 May 2023 15:36:47 +0800)
- 4274503 - chore: Change Python version from 3.7 to 3.9, add dev deps (Sun, 7 May 2023 15:34:36 +0800)
- 379c3ac - chore: Remove pt-datasets, numba, and llvmlite from deps, add sMCL package (Sun, 7 May 2023 15:33:05 +0800)
- 9ea7d0e - chore: export trained model (Fri, 3 Sep 2021 15:45:29 +0800)
- 9eb93ce - chore: parameterize dataset normalization (Wed, 25 Aug 2021 13:05:14 +0800)
- 49739b9 - chore: remove cuda index (Mon, 23 Aug 2021 23:46:23 +0800)
- b631fc5 - chore: remove AMP-mode for training (Mon, 23 Aug 2021 23:45:04 +0800)
- dccd7c4 - chore: add outputs dir to .gitignore (Mon, 23 Aug 2021 22:23:19 +0800)
- a0630ca - chore: remove unused declare vars (Tue, 17 Aug 2021 21:44:35 +0800)
- 7855bb5 - chore: add AdamW optimizer option (Tue, 17 Aug 2021 21:35:23 +0800)
- 96da7e7 - chore: switch device for subnetwork (Tue, 17 Aug 2021 21:34:18 +0800)
- 5669076 - chore: define function for ensemble filename (Thu, 5 Aug 2021 01:47:37 +0800)
- b329d5d - chore: add LeNet classifier module (Tue, 15 Jun 2021 18:44:23 +0800)
- b29b666 - chore: add flake8 config (Tue, 15 Jun 2021 18:31:15 +0800)
- 7e9b5b0 - chore: add pre-commit config (Tue, 15 Jun 2021 18:31:08 +0800)
- b9cc4ba - chore: use global seed setter function from own package (Mon, 14 Jun 2021 19:43:10 +0800)
- 57cebfe - chore: add function for setting global seed (Mon, 14 Jun 2021 19:42:12 +0800)
- 2b33572 - chore: add CNN sMCL classifier (Mon, 14 Jun 2021 19:33:43 +0800)
- 5ac404b - chore: perform lr decay at train phase (Fri, 11 Jun 2021 02:07:25 +0800)
- 096a8cd - chore: add LeNet model (Thu, 10 Jun 2021 12:37:03 +0800)
- 1463a78 - chore: fix minimum lr of ensemble to 1e-4 (Wed, 9 Jun 2021 22:49:17 +0800)
- 36d7446 - chore: add ensemble DNN classifier (Wed, 9 Jun 2021 22:47:30 +0800)
- 3d18147 - chore: add lr decay for ensemble (Sun, 30 May 2021 03:05:55 +0800)
- 6a2c28e - chore: add ensemble model (Sat, 29 May 2021 01:44:05 +0800)
- 0876c2d - chore: add CNN model (Fri, 28 May 2021 20:47:52 +0800)
- 84b3495 - chore: add DNN model (Sat, 22 May 2021 23:58:51 +0800)
- 66f252b - chore: add poetry file (Fri, 21 May 2021 18:36:01 +0800)
- 6339577 - chore: add package (Fri, 21 May 2021 18:34:28 +0800)

## Documentation

- 13068c7 - docs: Update license in pyproject.toml (Sat, 2 Mar 2024 13:33:42 +0800)
- f0b3d49 - docs: Add issue template (Sun, 7 May 2023 15:55:24 +0800)
- a4141be - docs: update class docstring (Mon, 23 Aug 2021 23:47:27 +0800)
- 7d3d96d - docs: add docstring for freezing/unfreezing losing learners (Thu, 5 Aug 2021 21:43:39 +0800)
- 81bcd36 - docs: add docstring for unfreezing losing learners (Thu, 5 Aug 2021 21:39:59 +0800)
- 3767215 - docs: add docstring for freezing losing learners (Thu, 5 Aug 2021 21:39:00 +0800)
- 3bee990 - docs: Migrate to AGPL v3 from GPL v2 license (Wed, 21 Jul 2021 08:46:59 +0000)
- 08f58b4 - docs: update boilerplate license (Fri, 21 May 2021 18:34:07 +0800)

## Features

- a59ff83 - feat: add option to normalize vision datasets (Wed, 25 Aug 2021 13:05:05 +0800)
- 42f21fa - feat: use pretrained CIFAR10 ResNet model as a feature extractor (Tue, 24 Aug 2021 12:31:28 +0000)
- 1054347 - feat: parameterize cuda device index number (Tue, 17 Aug 2021 16:55:15 +0800)
- ccf113e - feat: parameterize number of ResNet blocks to freeze (Tue, 17 Aug 2021 16:53:06 +0800)
- 3c4a334 - feat: use pretrained CIFAR10 ResNet models (Mon, 16 Aug 2021 15:27:26 +0000)
- 655c090 - feat: implement automatic mixed precision mode for training (Mon, 9 Aug 2021 08:50:09 +0000)
- 1979692 - feat: add functions for saving results and trained model (Wed, 4 Aug 2021 13:22:12 +0000)
- e6b23ac - feat: add function for splitting training data (Wed, 4 Aug 2021 13:16:55 +0000)

## Fixes

- 5e49245 - fix: pass device to use for subnetworks (Tue, 24 Aug 2021 00:25:22 +0800)
- dfb3c0f - fix: reset params only when not using ResNet (Tue, 17 Aug 2021 17:05:46 +0800)
- f280c32 - fix: instantiate CNN for ensemble (Mon, 14 Jun 2021 19:39:08 +0800)

## Improvements

## Refactors

- 1efe61a - refactor: rewrite classifier module (Wed, 4 Aug 2021 17:55:29 +0000)
- 0c45250 - refactor: remove retired function (Fri, 11 Jun 2021 02:06:56 +0800)
- 0adbdc8 - refactor: reformat file (Fri, 21 May 2021 18:33:50 +0800)

## Tests
